#include "Calculator.h"
#include <QApplication>

Calculator* calculator;

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    calculator = new Calculator();
    calculator->show();

    return a.exec();
}
