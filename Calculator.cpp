#include "Calculator.h"
#include <QSlider>
#include <QSpinBox>
#include <QHBoxLayout>
#include <QFont>
#include <QGraphicsTextItem>
#include <QComboBox>

Calculator::Calculator(QWidget *parent) {
    scene = new QGraphicsScene();
    scene->setSceneRect(0,0,1000,600);
    setBackgroundBrush(QBrush(QImage(":/images/bg.PNG")));

    setScene(scene);
    setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setFixedSize(1000,600);

    course = 1;
    hwGrade=0;
    midtermGrade=0;
    finalGrade=0;
    totalGrade=0;
    hSliders.assign(10,0);
    mSliders.assign(2,0);
    hSpinBox.assign(10,0);
    mSpinBox.assign(2,0);
    homework.assign(10,0);
    midterm.assign(2,0);

    start();
}

void Calculator::start() {
    c(10,2);
}

void Calculator::addCourse() {
    QGraphicsTextItem* course = new QGraphicsTextItem(QString("COURSE:"));
    int x = 30;
    int y = 25;
    course->setPos(x,y);
    course->setDefaultTextColor(Qt::darkCyan);
    course->setFont(QFont("helvetica",11));
    scene->addItem(course);

    QComboBox* menu = new QComboBox();
    menu->move(x+75,y+3);
    menu->addItem("CLASS 1");
    menu->addItem("CLASS 2");

    QObject::connect(menu,SIGNAL(activated(int)),this,SLOT(mySlot(int)));
    scene->addWidget(menu);
}

void Calculator::addHw(int num) {
    QGraphicsTextItem* hw = new QGraphicsTextItem(QString("HW ") +  QString::number(num));
    int y = num*50+25;
    hw->setPos(30,y);
    hw->setDefaultTextColor(Qt::darkCyan);
    hw->setFont(QFont("helvetica",10));
    scene->addItem(hw);

    QSlider* slider = new QSlider(Qt::Horizontal);
    slider->setRange(0,100);
    slider->move(100,y);

    hSliders[num-1] = slider;

    QSpinBox* spinbox = new QSpinBox();
    spinbox->setRange(0,100);
    spinbox->move(330,y);
    spinbox->setValue(0);

    hSpinBox[num-1] = spinbox;

    QObject::connect(slider,SIGNAL(valueChanged(int)),spinbox,SLOT(setValue(int)));
    QObject::connect(spinbox,SIGNAL(valueChanged(int)),slider,SLOT(setValue(int)));

    QObject::connect(slider,SIGNAL(valueChanged(int)),this,SLOT(recordHwSlider()));
    QObject::connect(spinbox,SIGNAL(valueChanged(int)),this,SLOT(recordHwSpinbox()));

    scene->addWidget(slider);
    scene->addWidget(spinbox);
}

void Calculator::addMidterm(int num) {
    QGraphicsTextItem* midterm = new QGraphicsTextItem(QString("MIDTERM ") +  QString::number(num));
    int x = 500;
    int y = num*100-30;
    midterm->setPos(x,y);
    midterm->setDefaultTextColor(Qt::darkCyan);
    midterm->setFont(QFont("helvetica",10));
    scene->addItem(midterm);

    QSlider* slider = new QSlider(Qt::Horizontal);
    slider->setRange(0,100);
    slider->move(x,y+30);

    mSliders[num-1] = slider;

    QSpinBox* spinbox = new QSpinBox();
    spinbox->setRange(0,100);
    spinbox->move(x,y+60);
    spinbox->setValue(0);

    mSpinBox[num-1] = spinbox;

    QObject::connect(slider,SIGNAL(valueChanged(int)),spinbox,SLOT(setValue(int)));
    QObject::connect(spinbox,SIGNAL(valueChanged(int)),slider,SLOT(setValue(int)));

    QObject::connect(slider,SIGNAL(valueChanged(int)),this,SLOT(recordMidtermSlider()));
    QObject::connect(spinbox,SIGNAL(valueChanged(int)),this,SLOT(recordMidtermSpinbox()));

    scene->addWidget(slider);
    scene->addWidget(spinbox);
}

void Calculator::addFinal(int num) {
    QGraphicsTextItem* midterm = new QGraphicsTextItem(QString("FINAL"));
    int x = 500;
    int y = (num+1)*100-30;
    midterm->setPos(x,y);
    midterm->setDefaultTextColor(Qt::darkCyan);
    midterm->setFont(QFont("helvetica",10));
    scene->addItem(midterm);

    QSlider* slider = new QSlider(Qt::Horizontal);
    slider->setRange(0,100);
    slider->move(x,y+30);

    QSpinBox* spinbox = new QSpinBox();
    spinbox->setRange(0,100);
    spinbox->move(x,y+60);
    spinbox->setValue(0);

    QObject::connect(slider,SIGNAL(valueChanged(int)),spinbox,SLOT(setValue(int)));
    QObject::connect(spinbox,SIGNAL(valueChanged(int)),slider,SLOT(setValue(int)));

    QObject::connect(slider,SIGNAL(valueChanged(int)),this,SLOT(recordFinal(int)));
    QObject::connect(spinbox,SIGNAL(valueChanged(int)),this,SLOT(recordFinal(int)));

    scene->addWidget(slider);
    scene->addWidget(spinbox);
}

void Calculator::recordHwSlider() {
    hwGrade = 0;
    int max = 0;
    if (course == 1) {
        max = 10;
    } else if (course == 2) {
        max = 5;
    }
    for(int i = 0; i < max; i++){
        homework[i] = hSliders[i]->value();
        hwGrade+= (homework[i]);
    }
    updateTotal();
    return;
}

void Calculator::recordHwSpinbox() {
    hwGrade = 0;
    int max = 0;
    if (course == 1) {
        max = 10;
    } else if (course == 2) {
        max = 5;
    }
    for(int i = 0; i < max; i++){
        homework[i] = hSpinBox[i]->value();
        hwGrade+= (homework[i]);
    }
    updateTotal();
    return;
}

void Calculator::recordMidtermSlider() {
    midtermGrade = 0;
    int max = 0;
    if (course == 1) {
        max = 2;
    } else if (course == 2) {
        max = 1;
    }
    for(int i = 0; i < max; i++){
        midterm[i] = mSliders[i]->value();
        midtermGrade+= (midterm[i]);
    }
    updateTotal();
    return;
}

void Calculator::recordMidtermSpinbox() {
    midtermGrade = 0;
    int max = 0;
    if (course == 1) {
        max = 2;
    } else if (course == 2) {
        max = 1;
    }
    for(int i = 0; i < max; i++){
        midterm[i] = mSpinBox[i]->value();
        midtermGrade+= (midterm[i]);
    }
    updateTotal();
    return;
}

void Calculator::recordFinal(int value) {
    finalGrade=value;
    updateTotal();
    return;
}

void Calculator::c(int hw, int mid) {
    addCourse();
    for (int i=1; i<=hw; i++) {
        addHw(i);
    }
    for (int i=1; i<=mid; i++) {
        addMidterm(i);
    }
    addFinal(mid);
    updateTotal();
}

void Calculator::removeItems() {
    QList<QGraphicsItem*> items = scene->items();
    for(int i=0, n=items.size(); i<n; ++i){
        if (typeid(items[i]) != typeid(menu) ) {
            scene->removeItem(items[i]);
        }
    }
    hwGrade=0;
    midtermGrade=0;
    finalGrade=0;
    totalGrade=0;
    hSliders.clear();
    mSliders.clear();
    hSpinBox.clear();
    mSpinBox.clear();
    homework.clear();
    midterm.clear();
    hSliders.assign(10,0);
    mSliders.assign(2,0);
    hSpinBox.assign(10,0);
    mSpinBox.assign(2,0);
    homework.assign(10,0);
    midterm.assign(2,0);

}

void Calculator::updateTotal() {
    QGraphicsRectItem* panel = new QGraphicsRectItem(500,430,200,120);
    QBrush brush;
    brush.setStyle(Qt::SolidPattern);
    brush.setColor(Qt::white);
    panel->setBrush(brush);
    scene->addItem(panel);

    QGraphicsTextItem* hgrade = new QGraphicsTextItem(QString("HOMEWORK GRADE: ") + QString::number(hwGrade));
    hgrade->setPos(520,440);
    hgrade->setDefaultTextColor(Qt::gray);
    hgrade->setFont(QFont("helvetica",9));
    scene->addItem(hgrade);

    QGraphicsTextItem* mgrade = new QGraphicsTextItem(QString("MIDTERM GRADE: ") + QString::number(midtermGrade));
    mgrade->setPos(520,460);
    mgrade->setDefaultTextColor(Qt::gray);
    mgrade->setFont(QFont("helvetica",9));
    scene->addItem(mgrade);

    QGraphicsTextItem* fgrade = new QGraphicsTextItem(QString("FINAL GRADE: ") + QString::number(finalGrade));
    fgrade->setPos(520,480);
    fgrade->setDefaultTextColor(Qt::gray);
    fgrade->setFont(QFont("helvetica",9));
    scene->addItem(fgrade);

    totalGrade = hwGrade + midtermGrade + finalGrade;
    QGraphicsTextItem* grade = new QGraphicsTextItem(QString("TOTAL GRADE: ") + QString::number(totalGrade));
    grade->setPos(520,510);
    grade->setDefaultTextColor(Qt::darkCyan);
    grade->setFont(QFont("helvetica",12));
    scene->addItem(grade);
}

void Calculator::mySlot(int arg) {
    if(arg==0) {
        removeItems();
        course = 1;
        c(10,2);
    } else if (arg==1) {
        removeItems();
        course = 2;
        c(5,1);
    }
}




