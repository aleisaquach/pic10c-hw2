#ifndef CALCULATOR_H
#define CALCULATOR_H

#include <QGraphicsView>
#include <QGraphicsScene>
#include <QSlider>
#include <QSpinBox>
#include <QComboBox>

class Calculator: public QGraphicsView{
    Q_OBJECT
public:
    Calculator(QWidget *parent=0);

    QGraphicsScene * scene;
    QComboBox* menu;
    QGraphicsTextItem* grade;

    void start();
    void addCourse();
    void addHw(int num);
    void addMidterm(int num);
    void addFinal(int num);
    void c(int hw, int mid);
    void removeItems();

private slots:
    void mySlot(int arg);
    void recordHwSlider();
    void recordMidtermSlider();
    void recordHwSpinbox();
    void recordMidtermSpinbox();
    void recordFinal(int value);

private:
    std::vector<QSlider*> hSliders;
    std::vector<QSlider*> mSliders;

    std::vector<QSpinBox*> hSpinBox;
    std::vector<QSpinBox*> mSpinBox;

    std::vector<int> homework;
    std::vector<int> midterm;

    int course;
    int hwGrade;
    int midtermGrade;
    int finalGrade;
    int totalGrade;
    void updateTotal();
};


#endif // CALCULATOR_H
