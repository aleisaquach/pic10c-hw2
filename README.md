# grade calculator
The purpose of this repository is to create an interactive grade calculator that responds to mouse click events. It calculates the sum of homework assignments, midterms, and a midterm.

It offers two different classes schemes: 

**OPTION 1:** 10 homework assignments, 2 midterms, 1 final

**OPTION 2:** 5 homework assignments, 1 midterm, 1 final

## video of working calculator
a simulation of the calculator can be found [here](https://drive.google.com/file/d/1ETyIfplnmOxjw8CbA6ist8dwmBvR6ppk/view?usp=sharing)

## process
While working on this project, I was able to quickly create an interafce that displayed the sliders and spinboxes. However, I had a hard time calculating the grade. Eventually, I was able to calculate the homework assigments, midterms, final, and total grade(sum).

I am still confused as of how to calculate the acutal grade or a weighted grade. When I divided the section grades and multiplied them by their respective weighting, the total grade at the end would not always show because of the rounding involved between integers and doubles. 

## files included
| file | description |
|------|-------------|
| [calculator.cpp](https://bitbucket.org/aleisaquach/pic10c-hw2/src/master/Calculator.cpp) | declaration of calculator member functions |
| [calculator.h](https://bitbucket.org/aleisaquach/pic10c-hw2/src/master/Calculator.h) | implements calculator | 
| [gradecalculator.pro](https://bitbucket.org/aleisaquach/pic10c-hw2/src/master/GradeCalculator.pro) | Qt file to keep track of project | 
| [gradecalculator.pro.user](https://bitbucket.org/aleisaquach/pic10c-hw2/src/master/GradeCalculator.pro.user) | user specific Qt file |
| [bg.PNG](https://bitbucket.org/aleisaquach/pic10c-hw2/src/master/bg.PNG) | blue blackground image for calculator |
| [main.cpp](https://bitbucket.org/aleisaquach/pic10c-hw2/src/master/main.cpp) | main file | 
| [mainwindow.ui](https://bitbucket.org/aleisaquach/pic10c-hw2/src/master/mainwindow.ui) | mainwindow | 
| [res.qrc](https://bitbucket.org/aleisaquach/pic10c-hw2/src/master/res.qrc) | resource file for background image |